/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rishchen <rishchen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 13:14:54 by rishchen          #+#    #+#             */
/*   Updated: 2017/12/08 19:43:18 by rishchen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*

================================= Mandatory =============================================
	1) You have to manage the following conversions: sSpdDioOuUxXcC
	2) You must manage %%
	3) You must manage the flags #0-+ and space
	4) You must manage the minimum field-width
	5) You must manage the precision
	6) You must manage the flags hh, h, l, ll, j, et z.
..........................................................................................

================================= Bonus ==================================================
1) More detailed conversions management: eE, fF, gG, aA, n.
2) More detailed flags management: *, $, L, ’.
3) Non-existing flags management: %b to print in binary, %r to print a string of non- printable characters, %k to print a date in any ordinary ISO format etc.
4) Management of alter tools for colors, fd or other fun stuff like that :)
..........................................................................................

*/

#include "ft_printf.h"
# include <stdio.h>
# include <string.h>

void	loop_for_wchar(wchar_t *str)
{
	int	i;

	unsigned	int	c;

	i = -1;
	while (str[++i])
	{
		c = (unsigned int)str[i];
		print_wchar(c);
	}
}

void	loop_for_char(char *str)
{
	int	i;

	unsigned	int	c;

	i = -1;
	while (str[++i])
	{
		c = (unsigned int)str[i];
		print_wchar(c);
	}
}

int		ft_printf(const char *fmt, ...)
{
	va_list	ap;
	t_data	data;
	int		res;

	va_start(ap, fmt);
	while (*fmt)
	{
		res = launch_parse(&fmt, &ap);
	}
	va_end(ap);

	// printf("RES = %d\n", res);

	return (0);
}

int		main()
{
	setlocale(P_ALL, "en_US.UTF-8");

	wchar_t *a = L"ю";

	wchar_t **b = &a;

	// printf("%c\n", (void *)0);
	printf("%1.32 10.12s\n", "123");
	ft_printf("%1.32  10.12s\n", "123");


	// ft_printf("%c", L'∑');
	// ft_printf("%s", L"qwe");

	// ft_printf("%S", L"ααα");

	// sleep(42);

	return (0);
}