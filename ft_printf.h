/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rishchen <rishchen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 13:30:07 by rishchen          #+#    #+#             */
/*   Updated: 2017/12/08 16:30:35 by rishchen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FTPRINTF_H
# define FTPRINTF_H

# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>

static const char flags[4] = {'+', '-', ' ', '0'};
static const char conversion[12] = {'d', 'D', 's', 'S', 'c', 'C', 'i', 'O', 'u', 'U', 'x', 'X'};
static const char control_chars[7] = {'a', 'b', 't', 'n', 'v', 'f', 'r'}; 

typedef int bool;
# define true 1
# define false 0

typedef struct	s_data
{
	size_t		width;
	int			double_perc;
	int			precision;
	int			position;
	char		flags;
	char		type;
	bool		is_valid;
}				t_data;

char		*ft_itoa_base(int value, int base);
void		f(int value, int base, char *str, int *i);
int			ft_atoi_base(char *str, int base);
static int	ft_inbase(char c, int base);

void		print_wchar(unsigned int symbol);
void		str_conversation(const char *fmt, va_list *ap);

void		loop_for_wchar(wchar_t *str);
void		loop_for_char(char *str);

size_t      ft_strlen(const char *s);
int			find_szie(wchar_t *str);
void		ft_putnbr(int n);

int	        launch_parse(const char** str, va_list *ap);
void		ft_bzero(void *s, size_t n);
int			ft_atoi(const char *str);
int			ft_isdigit(int c);
int			ft_isalpha(int c);
size_t      print_string(t_data *info, char *arg);
void		ft_putstr(char const *s);
char		*ft_strsub(char const *s, unsigned int start, size_t len);


/*
typedef     enum
{
    plus = '+',
    minus = '-',
    space = ' ',
    zero = '0'
}           e_flags;

typedef     enum
{
    d = 'd',
    D = 'D',
    s = 's',
    S = 'S',
	c = 'c',
	C = 'C',
	i = 'i',
	o = 'O',
	u = 'u',
	U = 'U',
	x = 'x',
	X = 'X'
}           e_conversion;

// if (**str == 'd' || **str == 's' || **str == 'D' || **str == 'S' || **str == 'c' || **str == 'C' 
//		|| **str == 'i' || **str == 'o' || **str == 'O' || **str == 'u' || **str == 'U' || **str == 'x' || **str == 'X' || **str == '\0')
*/
#endif