/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_string.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rishchen <rishchen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 14:48:53 by rishchen          #+#    #+#             */
/*   Updated: 2017/12/08 19:42:48 by rishchen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

// loop_for_char(s);

size_t      print_string(t_data *info, char *arg)
{
    size_t  len;
    size_t  print_len;
    size_t  i;
    unsigned int c;

    len = ft_strlen(arg);
    // Kostil?

    // if (info->width == 0 && info->precision != -42 )
    //     info->width = info->precision;
    // if (info->precision >= len)
    //     info->precision = len;
    info->precision = (info->precision >= len ? len : info->precision);
    info->width = ((info->width == 0 || info->width <= len) && info->precision != -42 ? info->precision : info->width);
    // info->width = (info->width <= len ? len : info->width);
    info->flags = (info->flags == '\0' ? ' ' : info->flags);

    if (info->width <= len && (info->precision == -42 || info->precision >= len))
    {
        ft_putstr(arg);
        return (len);
    }
    else
    {
        i = 0;
        if (len > (size_t)info->precision)// || info->width > info->precision)
            print_len = info->width - (size_t)info->precision;
        else
            print_len = info->width - len;
        if (info->flags == ' ' || info->flags == '0' || info->flags == '+')
        {
            info->flags = (info->flags == '+' ? ' ' : info->flags); 
            while (i++ < print_len)
                write(1, &(info->flags), 1);
            i = 0;
            while (i < (size_t)info->precision && i < len)
            {
                print_wchar(arg[i]);
                i++;
            }
        }
        else
        {
            while (i < (size_t)info->precision && i < len)
            {
                print_wchar(arg[i]);
                i++;
            }
            i = 0;
            while (i++ < print_len)
                write(1, " ", 1);
        }
    }

    return (info->width);
}

// size_t      print_string(t_data *info, char *arg)
// {
//     size_t  len;
//     size_t  print_len;
//     size_t  i;

//     len = ft_strlen(arg);
//     if (info->width <= len && info->precision == -42)
//     {
//         ft_putstr(arg);
//         return (len);
//     }
//     else
//     {
//         print_len = info->width - len;
//         // __builtin_printf("LEN = %d\n", len);
//         // return ;
//         i = 0;
//         if (info->flags == ' ' || info->flags == '0')
//         {
//             while (i++ < print_len)
//                 write(1, &(info->flags), 1);
//             ft_putstr(arg);
//         }
//         else
//         {
//             if (info->flags == '-')
//             {
//                 ft_putstr(arg);
//                 while (i++ < print_len)
//                     write(1, " ", 1);
//             }
//             else
//             {
//                 while (i++ < print_len)
//                     write(1, " ", 1);
//                 ft_putstr(arg);
//             }
//         }
//         return (info->width);
//     }
// }