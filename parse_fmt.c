/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_fmt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rishchen <rishchen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/02 14:34:30 by rishchen          #+#    #+#             */
/*   Updated: 2017/12/08 19:38:49 by rishchen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int         check_flag(char symbol)
{
    int     i;

    i = -1;
    while (flags[++i])
        if (flags[i] == symbol)
            return(1); 
    return (0);
}

int         check_conversion(char symbol)
{
    int     i;

    i = 0;
    while (conversion[i++])
        if (conversion[i] == symbol && symbol != '\0')
            return (1);
    return (0);
}

int         check_control_chars(char symbol)
{
    int     i;

    i = 0;
    while (control_chars[i++])
        if (control_chars[i] == symbol && symbol != '\0')
            return (i);
    return (-1);
}

int         init_data(char *str, t_data *info)
{
    int     i;
    int     j;
    int     num;
    int     trigger;
    int     minus;

    //eto prosto pizdec
    char    *str_sub;

    i = 0;
    trigger = 0;
    minus = 0;
    while (str[i] && !ft_isalpha(str[i]))
    {
        if (str[i] == '%')
            info->double_perc++;
        if (info->double_perc >= 2)
            return(info->is_valid);
        if (check_flag(str[i]) && !trigger)
        {
            if (minus && str[i] == '0')
                info->flags = '-';
            else
                info->flags = str[i];
            minus = (str[i] == '-' ? 1 : minus); 
        }
        else if (str[i] == '.')
        {
            num = 0;
            j = 0;
            while(ft_isdigit(str[++i]))
                j++;
            str_sub = ft_strsub(str, i - j, j);
            num = ft_atoi(str_sub);
            if (str_sub)
                free(str_sub);
            info->precision = num;
            i--;
        }
        else if (ft_isdigit(str[i]))
        {
            j = 0;
            while(ft_isdigit(str[i++]))
                j++;
            str_sub = ft_strsub(str, i - j - 1, j);
            num = ft_atoi(str_sub);
            if (str_sub)
                free(str_sub);
            info->width = (size_t)num;
            i -= 2;
            trigger = 1;
        }
        i++;
    }
    if (check_conversion(str[i]))
    {
        info->is_valid = true;
        info->type = str[i];
    }    
    else
        info->position = i;
    return (info->is_valid);
}

void        init_lexer_struct(t_data *info)
{
    info->flags = '\0';
    info->type = '\0';
    info->is_valid = false;
    info->position = 0;
    info->double_perc = 0;
    info->precision = -42;
    info->width = 0;
}

int        launch_parse(const char **str, va_list *ap)
{
    char    buff[1024];
    int     i;
    size_t  count_symbols;
    t_data  info;
    char	*s;
    int     control_char;

    count_symbols = 0;
    control_char = -1;
    //проходимся по всей строке и печатаем символы, пока не встретим '%'
    while (**str)
    {
        // сама разбивка по '%' и наполнение буффера, который будет обрабатываться
        if (**str == '%')
        {
            i = -1;
            ft_bzero(buff, 1024);
            // запихиваем все в буффер, пока не встретим один из флагов: sSpdDioOuUxXcC
            while (**str)
            {
                if (i < 1023)   
                    buff[++i] = **str;
                if (check_conversion(**str) || **str == '\0')
                    break;
                (*str)++;
            }
            // парсим буффер
            init_lexer_struct(&info);
            if (init_data(buff, &info))
            {
                if (info.type == 's' || info.type == 'S')
                    count_symbols += print_string(&info, va_arg(*ap, char *));
                if (**str)
                    (*str)++;
            }
            // else
            //     print_buffer(buff, info);
            // __builtin_printf("%zu %d %c %d\n", info.width, info.precision, info.flags, info.is_valid);
        }
        else
		{
            count_symbols++;
            write(1,*str, 1);
            if  (**str)
                (*str)++;
		}
    }
    return (count_symbols);
}