# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rishchen <rishchen@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/12/01 17:14:57 by rishchen          #+#    #+#              #
#    Updated: 2017/12/07 15:51:57 by rishchen         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_printf

SRC =	main.c \
		atoi_itoa.c \
		auxiliary_fuctions.c \
		print_chars.c \
		libft/ft_putchar.c \
		libft/ft_strlen.c \
		libft/ft_putnbr.c \
		libft/ft_putstr.c \
		libft/ft_bzero.c \
		libft/ft_atoi.c \
		libft/ft_isdigit.c \
		libft/ft_isprint.c \
		libft/ft_strsub.c \
		libft/ft_memcpy.c \
		libft/ft_strsplit.c \
		libft/ft_isalpha.c \
		parse_fmt.c \
		print_string.c

OBJ = $(SRC:.c=.o)
INC = -I libft/libft.h
LIBINC = -L./libft

LIBOBJ = make -C libft/

FLG = -Wall -Wextra -Werror

CC = gcc

all:	$(NAME)

%.o: %.c 
	$(CC) -g -c $<

$(NAME): liball $(OBJ)
	$(CC) $(OBJ) -o $(NAME)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

liball:
	@make -C libft/ all

libclean:
	@make -C libft/ clean

libfclean:
	@make -C libft/ fclean

libre: libfclean liball
